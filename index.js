// console.log(`hello world`)

// [Section] JSON Objects
/*
	-JSON stands for Javascript Object Notation.
	-JSON is also used in other programming languages. hence, the name JSON.
	-Core JavaScript has a built in JSON objects that contains methods for JSON objects and converting strings into javascript objects.
	-JSON is used for serializing different data types.
	Syntax:
		{
			"PropertyA": "valueA",
			"PropertyB": "valueB"
		}
*/

// JSON Object
/*{
	"city": "Quezon city",
	"province": "Metro Manila",
	"country": "Philippines"
}*/

// JSON Arrays:

/*[
	{"city": "Quezon city",
	"province": "Metro Manila",
	"country": "Philippines"},
	{"city": "Manila City",
 	"province": "Metro Manila",
 	"country": "Philippines"},
]*/

// [Section] JSON Methods
	// JSON object contains method for parsing and converting data into stringified JSON

let batchesArr = [
					{
						batchName: "Batch X"
					},
					{
						batchName: "Batch Y"
					}
				 ];
console.log(`This is the original array`)
console.log(batchesArr)
// The stringify method is used to convert JavaScript objects into a string
console.log(`Result from stringify method:`)
let stringBatchesArr = JSON.stringify(batchesArr);
console.log(stringBatchesArr)
console.log(typeof stringBatchesArr)

let data = JSON.stringify({
	name: 'John',
	address:{
		city: 'Manila',
		country: 'Philippines'
	}
})
console.log(data)


// [Section] using stringify method with variables
// when info is stored in a variable and is not hard coded into an object that is being stringified, we can supply the value with a variable

// user details
/*let firstName = prompt("What is your first name?");
let lastName = prompt("What is your last name?");
let age = prompt("How young are you?");
let address = {
	city: prompt("Which city do you live in?"),
	country: prompt("Which country does your city address belong to?")
}

let otherData = JSON.stringify({
	firstName,
	lastName,
	age,
	address,
})
console.log(otherData)*/

// [Section] Conveting Stringify JSON into JavaScript object
/*
	-objects are common data types used in application because of the complex data structures that can be created out of them
	-information is commonly sent to applications in stringified JSON and then converted back into objects
	-This happens both for sending information to a backend application and sending information back to a front end application
*/

let batchesJSON = `[{"batchName":"Batch X"},{"batchName":"Batch Y"}]`;
let parseBatchesJSON = JSON.parse(batchesJSON);
console.log(parseBatchesJSON);


let stringifiedObject = `{
	"name" : "John",
	"age" : "31",
	"address" : {
		"city" :"Manila",
		"country" : "Philippines"
	}
}`

console.log(JSON.parse(stringifiedObject))